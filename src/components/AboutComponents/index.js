import React from "react"
import WelcomePage from "./WelcomePage"
import EducationBackground from "./Education"
import DocumentRepresentation from "./Documents"
import ContactInfo from "./ContactInfo"
export default function AboutMeComponents(){
    return (
        <div>
            <WelcomePage />
            <EducationBackground />
            <DocumentRepresentation />
            <ContactInfo />
        </div>
    )
}