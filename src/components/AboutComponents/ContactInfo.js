import React from "react"
import {Link} from "react-router-dom"

export default function ContactInfo(){
    return (
      <div className="contact-infos" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
              <h2>My contact Info,<br />Here we go ,reache me</h2>
          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
           <ul>
               <li>Email : ntwariegide2@gmail.com</li>
               <li>Phone : +250 783 164 971</li>
               <li>Whatsapp : +250 783 164 971</li>
               <li>LinkedIn : ntwari egide</li>
           </ul>
           <div className="d-lg-flex">
                <Link to="#contact" className="my-button">contact me</Link>
            </div>
          </div>
        </div>
      </div>
    )
}