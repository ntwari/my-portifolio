import React from "react"
import {Link} from "react-router-dom"

export default function DocumentRepresentation(){
    return (
        <div className="education-background" data-aos="fade-up">
            <h1>Featured Documents</h1>
            <div className="worning-msg">
            <p>Ooops , TypeScript is failing to render these documents due to low network connection<br/>You can access the files below using the link on <strong>credentials</strong></p>
            </div>
            <div className="row">
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
            <iframe src="https://docs.google.com/document/d/1pp_HSpSiOKsbRZRGuCCL46kdKHd4mbwLFkCDIYbaBu4/edit#"></iframe>
            <p>Cridentials: <Link to="//docs.google.com/document/d/1pp_HSpSiOKsbRZRGuCCL46kdKHd4mbwLFkCDIYbaBu4/edit#" target="_blank">Open My Cv</Link> </p>
                <p class="level">Document : <span>My Cv File</span></p>
                <p class="status">Updated : <span class="text-success">2<supscript>nd</supscript>, Oct 2020</span></p>                
            </div>
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <iframe src="https://docs.google.com/document/d/1pp_HSpSiOKsbRZRGuCCL46kdKHd4mbwLFkCDIYbaBu4/edit#"></iframe>
            <p>Cridentials: <Link to="//docs.google.com/document/d/1pp_HSpSiOKsbRZRGuCCL46kdKHd4mbwLFkCDIYbaBu4/edit#" target="_blank">Open My Resume</Link> </p>
                <p class="level">Document : <span>My Resume File</span></p>
                <p class="status">Updated : <span class="text-success">3<supscript>rd</supscript>, Oct 2020</span></p>
            </div>
            </div>
        </div>
    )
}