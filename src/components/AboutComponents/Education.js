import React from "react"
import RcaImage from "./../../images/rca.jpg"
import GahiniImange from "./../../images/gahini.jpg"

export default function EducationBackground(){
    return (
        <div className="education-background" data-aos="fade-up">
            <h1>Edication background</h1>
            <div className="row">
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <h2>Rwanda Coding Academy</h2>
                <img src={RcaImage} />
                <p class="description">
                With a motto ‘Born to Code’ the Rwanda Coding Academy was purposely built in Mukamira sector, Nyabihu district, Western Province due to the region’s favorable education environment <br />
                The mission of the Rwanda coding academy is to <strong>train young talented and gifted Rwandans</strong> in software programing, promote quality and excellence in coding skills and to position Rwanda as software development talent hub
                </p>
                <p class="level">Level : <span>Bachelor's Degree</span></p>
                <p class="location">Location : Nyabihu - West - Rwandan</p>
                <p class="time">Time : 2019 - Current</p>
                <p class="status">Status : <span class="text-success">Currently Learning</span></p>                
            </div>
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <h2>Gahini Secondary School</h2>
                <img src={GahiniImange} />
                <p class="description">
                Groupe Scolaire De Gahini is located in Gahini. The school is listed as a Public school in the country of Rwanda. The Address of this secondary school is Kayonza District, Gahini <br />
                Good School near lake Muhazi ,favourable climate,with best teachers who teachs you all foundamentals that is required to build your future career <br />
                <strong>I salute you my school</strong>,my parent ,my guider <strong>Gahini</strong>
                </p>
                <p class="level">Level : <span>Ordinary level</span></p>
                <p class="location">Location: Kayonza - East - Rwandan</p>
                <p class="time">Time : 2016 - 2018</p>
                <p class="status">Status : <span class="text-info">Graduated</span></p>                
            </div>
            </div>
        </div>
    )
}