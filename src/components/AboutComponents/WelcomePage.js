import React from "react"
import {Link } from "react-router-dom"

export default function WelcomePage(){
    return (
        <section id="about" className="about">
      <div className="welcome-content" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h2>Regarding Me</h2>
            <p>
              I am Ntwari Egide, software engineer carrently learning at Rwanda Coding Academy with about 2- years experience in coding career,willing to be one of young IT entrepreneur , I am expert in building web apps, best designer with best upcoming trends,skilled in Agille methodology
            </p>
            <p class="written-by">- ntwari egide</p>
          </div>
        </div>

      </div>
    </section>
    )
}