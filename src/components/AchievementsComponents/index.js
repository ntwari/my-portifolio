import React from "react"
import WelcomePage from "./WelcomePage"
import CertificatePresentation from "./Certificates"
export default function AchievementsComponents(){
    return (
        <div>
            <WelcomePage />
            <CertificatePresentation />
        </div>
    )
}