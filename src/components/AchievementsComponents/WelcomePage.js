import React from "react"
import MobileImg from "./../../images/achievementsCover-mobile.png"

export default function WelcomePage(){
    return (
        <div id="achievements">
            <img src={MobileImg} alt="" />
            <h2>My Achivements</h2>
            <p>
            The best programs are written so that computing machines can perform them quickly and so that human beings can understand them clearly, a language that doesn't affect the way you think about programming is not worth knowing
            <br />
            I'm not a great programmer; I'm just a good programmer with great habits,Truth can only be found in one place: my codes
            </p>
        </div>
    )
}