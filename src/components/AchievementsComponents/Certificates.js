import React from "react"
import {Link} from "react-router-dom"

export default function CertificatePresentation(){
    return (
        <div className="education-background" data-aos="fade-up">
        <h1>My certificates</h1>
        <div className="worning-msg">
            <p>Ooops , TypeScript is failing to render these certificates due to low network connection<br/>You can access certificates below using the link on <strong>credentials</strong></p>
        </div>
        
        <div className="row">
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <h2>UX Designing Certificate</h2>
                <iframe src="https://www.futurelearn.com/certificates/vfvlmpv"></iframe>
                <p>Cridentials: <Link to="//www.futurelearn.com/certificates/vfvlmpv" target="_blank">Open Certificate</Link> </p>
                <ul>
                    <li>The foundations of UX design</li>
                    <li>User journeys</li>
                    <li>Visual design</li>
                    <li>Wireframes and interactive prototypes</li>
                    <li>User testing</li>
                    <li>Interaction design</li>
                    <li>Analysing data</li>
                </ul>
                <p class="issued-by">Issued by : <span>Future Learn</span></p>
                <p class="time">Time :<span> Oct,2020 - Current</span></p>             
            </div>
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
            <h2>Google Web Developer Certificate</h2>
                <iframe src="https://skillshop.exceedlms.com/student/award/58994036?referer=https%3A%2F%2Fskillshop.exceedlms.com%2Fstudent%2Fpath%2F1025-google-web-designer-basics"></iframe>

                <p>Cridentials: <Link to="//skillshop.exceedlms.com/student/award/58994036?referer=https%3A%2F%2Fskillshop.exceedlms.com%2Fstudent%2Fpath%2F1025-google-web-designer-basics" target="_blank">Open Certificate</Link> </p>      
                <ul>
                    <li> build engaging creatives web design</li>
                    <li>Visual Designing</li>
                    <li>Dark Mode Designing</li>
                    <li>3D Editing</li>
                </ul>
                <p class="issued-by">Issued by : <span>Google llc</span></p>
                <p class="time">Time :<span> sept,2020 - Current</span></p>              
            </div>
        </div>


        <div className="row">
        <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
            <h2>Web Design Ultimite Guide Certificate</h2>
            <iframe src="https://drive.google.com/file/d/1gBYx54DRqeSMIwqFssCczt3Cmr60Jwkh/view?usp=sharing"></iframe>

            <p>Cridentials: <Link to="//drive.google.com/file/d/1gBYx54DRqeSMIwqFssCczt3Cmr60Jwkh/view?usp=sharing" target="_blank">Open Certificate</Link> </p>
            
            <ul>
                <li>Consistency of website</li>
                <li>Colours and imagery designing</li>
                <li>Building functionality</li>
                <li>User Journey</li>
                <li>Information architecture</li>
                <li>Evaluation and launch</li>
            </ul>
            <p class="issued-by">Issued by : <span>BitDegree</span></p>
            <p class="time">Time :<span> Sept,2020 - Current</span></p>             
        </div>
        <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
        <h2>Scrum Foundation Professional Certificate (SFPC)</h2>
            <iframe src="https://certificates.easy-lms.com/exam/session/af696284-60c0-4d62-a6c9-494759444a7c"></iframe>
            
            <p>Cridentials: <Link to="//certificates.easy-lms.com/exam/session/af696284-60c0-4d62-a6c9-494759444a7c" target="_blank">Open Certificate</Link> </p>
            <ul>
                <li>Share Experiences and Encourages Collaboration</li>
                <li>Facilitate meetings (daily scrum, sprint planning, sprint demo and retrospective)</li>
                <li>Creating communication channel (team, Product owner, stakeholder)</li>
                <li>Coaching Team Development</li>
                <li>Flexibility and Persistence</li>
                <li>Partnering with the Product Owner</li>
                <li>Wanted and Dispensable</li>
                <li>Optimism and Servant Leadership</li>
                <li>Scrum Succeess values: commitment, courage, focus, openness and respect</li>
            </ul>
            <p class="issued-by">Issued by : <span>CertiProf</span></p>
            <p class="time">Time :<span> sept,2020 - Current</span></p>              
        </div>
        </div>

        <div className="row">
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <h2>Java Certificate of Completion</h2>
                <iframe src="https://www.sololearn.com/Certificate/1068-19537477/pdf/"></iframe>
                
                <p>Cridentials: <Link to="//www.sololearn.com/Certificate/1068-19537477/pdf/" target="_blank">Open Certificate</Link> </p>
                <ul>
                    <li>Java Basics</li>
                    <li>Object Oriented Programming language</li>
                    <li>JavaServer pages (JSP) and servlets</li>
                    <li>String handling</li>
                    <li>Collection framework</li>
                    <li>Multithreading</li>
                    <li>Exception handling</li>
                    <li>Generics</li>
                    <li>Synchronization</li>
                    <li>Internationalization</li>
                    <li>Service-oriented architecture/web services (SOAP/REST)</li>
                </ul>
                <p class="issued-by">Issued by : <span>SoloLearn</span></p>
                <p class="time">Time :<span> Aug,2020 - Current</span></p>             
            </div>
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
            <h2>React,Redux Certificate of Completion</h2>
                <iframe src="https://www.sololearn.com/Certificate/1097-19537477/pdf/"></iframe>
                
                <p>Cridentials: <Link to="//www.sololearn.com/Certificate/1097-19537477/pdf/" target="_blank">Open Certificate</Link> </p>
                <ul>
                    <li>JVX</li>
                    <li>Components</li>
                    <li>One-way Data Binding</li>
                    <li>Virtual DOM</li>
                    <li>Simplicity</li>
                    <li>Redux</li>
                    <li>Routing</li>
                    <li>API intergration</li>
                </ul>
                <p class="issued-by">Issued by : <span>SoloLearn</span></p>
                <p class="time">Time :<span> Aug,2020 - Current</span></p>              
            </div>
        </div>

        <div className="row">
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
                <h2>Transact SQL Certificate</h2>
                <iframe src="https://alison.com/certification/check/%242y%2410%249GENSIK8cRD2Yj5nJsvi2.DZ.x2A6nviPMmYVuIvnmGwT4dr0gl7."></iframe>
                
                 <p>Cridentials: <Link to="//alison.com/certification/check/%242y%2410%249GENSIK8cRD2Yj5nJsvi2.DZ.x2A6nviPMmYVuIvnmGwT4dr0gl7." target="_blank">Open Certificate</Link> </p>
                <ul>
                    <li>Working with Subqueries, Table Expressions and Data</li>
                    <li>Modifying Data and Using T-SQL in Programming</li>
                    <li>The common table expressions(CTEs) in SQL queries</li>
                    <li>All CRUD Operations</li>
                    <li>Error handling</li>
                    <li>Modifying Data and Using T-SQL in Programming</li>
                    <li>Pivoting data and unpivoting data</li>
                </ul>
                <p class="issued-by">Issued by : <span>Alison</span></p>
                <p class="time">Time :<span> sept,2020 - Current</span></p>             
            </div>
            <div className="col-lg-5 pt-4 pt-lg-0 content container" data-aos="zoom-in" data-aos-delay="200">
            <h2>SQL Certificate Of Completion</h2>
                <iframe src="https://www.sololearn.com/Certificate/1060-19537477/pdf/"></iframe>
                
                <p>Cridentials: <Link to="//www.sololearn.com/Certificate/1060-19537477/pdf/" target="_blank">Open Certificate</Link> </p>
                <ul>
                    <li>Functions, Stored Procedures, Packages</li>
                    <li>Pivoting data: CASE , PIVOT syntax</li>
                    <li>Hierarchical Queries</li>
                    <li>Cursors: Implicit and Explicit</li>
                    <li>Triggers</li>
                    <li>Dynamic SQL</li>
                    <li>Materialized Views</li>
                    <li>Query Optimization: Indexes</li>
                </ul>
                <p class="issued-by">Issued by : <span>SoloLearn</span></p>
                <p class="time">Time :<span> Aug,2020 - Current</span></p>              
            </div>
        </div>
    </div>
    )
}