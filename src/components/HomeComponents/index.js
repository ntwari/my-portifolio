import React from "react"
import WelcomePage from "./WelcomePage"
import WhatIcan from "./WhatIcanPage"
import WhatIamExperiencedIn from './WhatIexperience'
import ContactPage from "./ContactPage"

// constructor(props) {
//     super(props)
//     this.myRef = React.createRef()   // Create a ref object 
// }

// componentDidMount() {
//   this.myRef.current.scrollTo(0, 0);
// }

export default function HomeIndex(){
    return (
        <div>
            <WelcomePage />  
            <WhatIcan />
            <WhatIamExperiencedIn />
            <ContactPage />
        </div>
    );
}