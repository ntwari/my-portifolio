import React from "react"
import {Link} from "react-router-dom" 


export default function WhatIamExperiencedIn(){
    return (
        <section id="skills" className="skills what-experience">
      <div className="container" data-aos="fade-up">

        <div className="row">
            <div className="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3>What I am experienced in</h3>
            <p id="mg-experience">
            I have experience of about 2 years in software engineering ,I can design goot UX and UI of web apps, develop apps with different upcoming trending technology ,Scrum master in different projects
            </p>
            <div className = "container mg-commenting">
            Good decisions come from experience, and experience comes from bad decisions,my 2 - years of experience in coding career has tought me alot.
            </div>
            
          </div>
          <div className="col-lg-6 d-flex align-items-center" data-aos="zoom-in" data-aos-delay="100">
          <div className="skills-content">
            <div className="progress">
                <span className="skill">System Designer </span>
                <div className="progress-bar-wrap">
                    <div className="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <i className="fa fa-comments" aria-hidden="true"></i>

                <div className="progress">
                <span className="skill">System Developer </span>
                <div className="progress-bar-wrap">
                    <div className="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                
                </div>

                <div className="progress">
                <span className="skill">Scrum Master </span>
                <div className="progress-bar-wrap">
                    <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                </div>

                </div>
          </div>
          
        </div>

      </div>
    </section>
    )
}