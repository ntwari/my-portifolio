import React from "react"
import WhatIcanGif from "./../../images/whatIcan.gif"
import {Link} from "react-router-dom" 


export default function WhatIcanPage(){
    return (
        <section id="skills" className="skills">
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src={WhatIcanGif} className="img-fluid" alt="" />
          </div>
          <div className="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3>What I can</h3>
            <p>
              In market place , I am good at working with big groups and team leads , Aiming best User Interface and Experience for Rwandan IT Solutions and accross the world, best source code management using Git repositories in order to produce <strong>sheapable product</strong>
            </p>

            <ul>
                <li><i class="icofont-check-circled"></i> Descovering</li>
                <li><i class="icofont-check-circled"></i> Describing</li>
                <li><i class="icofont-check-circled"></i> Designing</li>
                <li><i class="icofont-check-circled"></i> Developing</li>
                <li><i class="icofont-check-circled"></i> Deploying</li>
            </ul>
            <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Reach Me</Link>
                </div>

          </div>
        </div>

      </div>
    </section>
    )
}