import React from "react"
import {Link} from "react-router-dom" 
import WelcomeGif from "./../../images/welcome.gif"

export default function WelcomePage(){
    return(
        <section id="hero" className="d-flex align-items-center">
            <div className="container">
            <div className="row">
                <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200"><br /><br /><br />
                <h1>Hello, <br />I am Ntwari Egide</h1>
                <h2>
                I'm  a <strong>software developer</strong> that utilize expertise in <strong>software development and testing</strong>, as well as deployment  using <strong>agile development methodologies</strong>. Coming with well honed skills in computer programming and additional excellent communication skills. 
                </h2>
                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started">Get In Touch</Link>
                </div>
                </div>
                {/* <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                    <img src={WelcomeGif} className="img-fluid animated" alt="" />
                </div> */}
            </div>
            </div>

        </section>
    );
}