import React from "react"
import LogoImg from "./../../images/logo.png"

export default function FooterComponent(){
    return(
        <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <img src={LogoImg} alt="" />
            <p>
              Kayonza, East,Rwanda<br />
              Rwanda<br /><br />
              <strong>Phone:</strong> +250 783 164 971<br />
              <strong>Email:</strong> ntwariegide2@gmail.com<br />
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>My Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div id ="contact" class="col-lg-3 col-md-6 footer-links">
            <h4>Contact Me</h4>
            <form>
                <input type="email" placeholder="Email Adress" /><br />
                <input type="text" placeholder="Subject" /><br />
                <textarea class="form-control" name="message" rows="4" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>My Social Networks</h4>
            <p>
                For more ideas, projects and other services, you can my social medias linked in ,twitter , instagram,facebook ,whatsapp
            </p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>ntwari egide</span></strong>. All Rights Reserved
      </div>
      <div class="credits">Designed by Ntwari Egide
      </div>
    </div>
  </footer>
    )
}