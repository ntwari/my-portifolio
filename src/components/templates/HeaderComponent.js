import React from "react"
import { Link } from "react-router-dom"
import logo from "./../../images/logo.png"

export default function HeaderComponent(){
    return (
    <header id="header" className="fixed-top ">
        <div className="d-flex align-items-left">

        {/* <h1 className="logo mr-auto"><Link to="index.html">Arsha</Link></h1> */}
        <Link to="/" className="logo mr-auto">
            <img src={logo} alt="" />
        </Link>

        <nav className="nav-menu d-none d-lg-block">
            <ul>
            <li><Link to="/achievements">My Achievements</Link></li>
            <li><Link to="/skills">My Skills</Link></li>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about-me">About Me</Link></li>
            </ul>
        </nav>

        <Link to="#contact" className="header-button get-started-btn scrollto">Get In Touch</Link>
    </div>
    </header>
    );
}