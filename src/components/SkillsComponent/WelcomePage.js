import React from "react"
import {Link} from "react-router-dom" 
import WelcomeGif from "./../../images/skillsWelcomePage.PNG"


export default function WelcomePage(){
    return (
        <section id="skillsWelcome">
            <div>
            <div className="row">
                <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                <h1>My Skills</h1>
                <h2>I quickly came to understand that code is a superpower every youth should be able to access. Understanding that code is the underlying (and invisible) <strong>framework of tech </strong> means that we do not have to be passive bystanders in our <strong>ever-changing digital world</strong></h2>

                {/* <h2>I came in market place with <strong>near zero programming</strong> knowledge and halfway in,But now I’m quite confident of what I can achieve</h2> */}
                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">my achievements</Link>
                </div>
                </div>
                <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                    <img src={WelcomeGif} className="img-fluid" alt="" />
                </div>
            </div>
            </div>

        </section>
    )
}