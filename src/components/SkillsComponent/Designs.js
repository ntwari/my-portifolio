import React from "react"
import {Link} from "react-router-dom" 
import SpringJpaImage from "./../../images/spring-data-jpa.png"
import NodejsImage from "./../../images/nodejs.png"
import PhpImage from "./../../images/php.png"
import ReactImage from "./../../images/react.png"
import VueImage from "./../../images/Vue.png"
import HtmlImage from "./../../images/html.png"
import BootstrapImage from "./../../images/bootstrap.png"
import MernImage from "./../../images/mern.png"
import MevnImage from "./../../images/mevn.png"
import SpringReactImage from "./../../images/spring-react.png"
import AdobeXdImage from "./../../images/adobeXd.png"
import PsImage from "./../../images/ps.png"
import FigmaImage from "./../../images/figma.png"
import ScrumImage from "./../../images/scrum.png"

export function BackEndDeveloperSkills() {
    return (
        <section id="firstDesign">
            <div>
            <div className="row">
                <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1 skills-desc" data-aos="fade-up" data-aos-delay="200">
                <h2>BackEnd Development</h2>
                <p>Well organized, detail oriented, ability to multi-task, excellent time-management,Proficient in the use of version control / source code management tools</p>

                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Hire me</Link>
                </div>
                </div>
                <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <div className="container">
                    <div className="skills row row-cols-2">
                        <div className="col align-item-center">
                            <img src={SpringJpaImage} alt="" />
                            <p>Spring Boot,JPA</p>
                        </div> 
                        <div className="col">
                            <img src={NodejsImage} alt="" />
                            <p>Node Js,Express</p>
                        </div> 
                        <div className="col">
                            <img src={PhpImage} alt="" />
                            <p>Php</p>
                        </div>                        
                    </div>
                    </div>
                </div>
            </div>
            </div>

        </section>
    )
    }

export function FrontEndDeveloperSkills() {
    return (
        <section id="firstDesign">
            <div>
            <div className="row">
            
            <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-1 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                <div className="container">
                    <div className="skills row row-cols-2">
                        <div className="col align-item-center">
                            <img src={ReactImage} alt="" />
                            <p>React, Redux</p>
                        </div> 
                        <div className="col">
                            <img src={BootstrapImage} alt="" />
                            <p>Bootstrap</p>
                        </div> 
                        <div className="col">
                            <img src={VueImage} alt="" />
                            <p>Vue, Vuex</p>
                        </div>
                        <div className="col">
                            <img src={HtmlImage} alt="" />
                            <p>HTML 5,CSS 6</p>
                        </div>                        
                    </div>
                    </div>
                </div>
                <div className="content-tobe-center col-lg-6 order-2 order-lg-2 hero-img justify-content-center skills-desc" data-aos="zoom-in" data-aos-delay="200">
                <h2>FrontEnd Developer</h2>
                <p>
                implementing visual elements that users see and interact with,Version Control/Git ,Responsive Design,Testing/Debugging,Browser developers tools
                </p>

                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Hire me</Link>
                </div>
                </div>
            </div>
            </div>

        </section>
    )
}

export function FullStackDeveloperSkills() {
    return (
        <section id="firstDesign">
            <div>
            <div className="row">
                <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1 skills-desc" data-aos="fade-up" data-aos-delay="200">
                <h2>Full Stack Developer</h2>
                <p>
                    I can tackle projects that involve databases, building user-facing websites, or even work with clients during the planning phase of projects.
                </p>

                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Hire me</Link>
                </div>
                </div>
                <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <div className="container">
                    <div className="skills row row-cols-2">
                        <div className="col align-item-center">
                            <img src={MevnImage} alt="" />
                            <p>MEVN</p>
                        </div> 
                        <div className="col">
                            <img src={SpringReactImage} alt="" />
                            <p>Spring Boot + React</p>
                        </div> 
                        <div className="col">
                            <img src={MernImage} alt="" />
                            <p>MERN</p>
                        </div>                        
                    </div>
                    </div>
                </div>
            </div>
            </div>

        </section>
    )
}

export function UiUxDesignerSkills() {
    return (
        <section id="firstDesign">
            <div>
            <div className="row">
            
            <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-1 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                <div className="container">
                    <div className="skills row row-cols-2">
                        <div className="col align-item-center">
                            <img src={AdobeXdImage} alt="" />
                            <p>Adobe XD</p>
                        </div> 
                        <div className="col">
                            <img src={PsImage} alt="" />
                            <p>Adobe Photoshop</p>
                        </div> 
                        <div className="col">
                            <img src={FigmaImage} alt="" />
                            <p>Figma</p>
                        </div>                        
                    </div>
                    </div>
                </div>
                <div className="content-tobe-center col-lg-6 order-2 order-lg-2 hero-img skills-desc" data-aos="zoom-in" data-aos-delay="200">
                <h2>UI UX Designer</h2>
                <p>
                    Design creates culture. Culture shapes values. Values determine the future, Designing product that is respectful, generous and helpful is my best aim
                </p>

                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Hire me</Link>
                </div>
                </div>
            </div>
            </div>

        </section>
    )
}

export function ScrumMasterSkills() {
    return (
        <section id="firstDesign">
            <div>
            <div className="row">
                <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1 skills-desc" data-aos="fade-up" data-aos-delay="200">
                <h2>Scum Master</h2>
                <p>
                Team performance is directly proportional to team stability. Focus on building and maintaining a stable team. Stability reduces friction and increases credibility and confidence
                </p>
                <p>I am able to manage scrum team leads, organizing sprint meeting and spring reviews,team should be commitment, courage, focus, openness and respect in order to succeed 
                </p>

                <div className="d-lg-flex">
                    <Link to="#contact" className="btn-get-started scrollto">Hire me</Link>
                </div>
                </div>
                <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <div className="container">
                    <div className="skills row row-cols-2">
                        <div className="col align-item-center">
                            <img src={ScrumImage} alt="" />
                            <p>Scrum Methodology</p>
                        </div>                         
                    </div>
                    </div>
                </div>
            </div>
            </div>

        </section>
    )
}