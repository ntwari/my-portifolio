import React from "react"
import WelcomePage from "./WelcomePage"
import {BackEndDeveloperSkills} from "./Designs"
import {FrontEndDeveloperSkills} from "./Designs"
import {FullStackDeveloperSkills} from "./Designs"
import {UiUxDesignerSkills} from "./Designs"
import {ScrumMasterSkills} from "./Designs"

export default function SkillsPage(){
    return (
        <div>
            <WelcomePage />
            <BackEndDeveloperSkills />
            <FrontEndDeveloperSkills />
            <FullStackDeveloperSkills />
            <UiUxDesignerSkills />
            <ScrumMasterSkills />
        </div>
    )
}