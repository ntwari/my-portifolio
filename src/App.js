import React from 'react';
import './App.css';
import './responsive.css';
import './main.js';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router,Route } from "react-router-dom"
import HeaderComponent from "./components/templates/HeaderComponent"
import FooterComponent from "./components/templates/FooterComponent"
import HomeIndex from './components/HomeComponents';
import SkillsPage from './components/SkillsComponent';
import AboutMeComponents from './components/AboutComponents';
import AchievementsComponents from './components/AchievementsComponents';
import ScrollMemory from 'react-router-scroll-memory';

function App() {
  return (
      <Router>
        <ScrollMemory />
        <div className="App">
          <HeaderComponent />          
          <Route exact path="/"> <HomeIndex /></Route>
          <Route path="/skills"><SkillsPage /></Route>
          <Route path="/about-me"><AboutMeComponents /></Route>
          <Route path="/achievements"><AchievementsComponents /></Route>          
          <FooterComponent />
        </div>
      </Router>
    );
}

export default App;
